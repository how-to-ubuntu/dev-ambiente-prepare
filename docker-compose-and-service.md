### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Desabilitar o início automáGico do serviço docker

Eu prefiro iniciar o serviço do docker somente quando eu realmente preciso utilizá-lo.

<br>

## Desabilitar início dos serviços docker

Para desabilitar o início do serviço junto com a inicialização do sistema:

```sh
sudo systemctl disable docker.service docker.socket
```
`~output`
```sh
Removed /etc/systemd/system/sockets.target.wants/docker.socket.
Removed /etc/systemd/system/multi-user.target.wants/docker.service.
```

<br>

## Executar serviços docker

Assim quando eu precisar executar um container, basta iniciar com o comando:

```sh
sudo service docker start
```

<br>

## Interromper serviços docker

E quando eu não precisar mais do serviço, basta encerrar com o comando:

```sh
sudo service docker stop
```

<br>

## Habilitar início dos serviços docker

Para habilitar novamente o início automáGico do serviço:

```sh
sudo systemctl enable docker.service docker.socket
```

<br>

# Impedir o docker de alterar diretamente o iptables

É importante também garantir que o docker exponha as portas da estação onde o serviço estiver rodando, seja ele uma estação de trabalho ou servidor.

Para garantir que o docker não exponha as portas do container, a configuração é simples!
Basta editar o arquivo `/etc/default/docker` e inclur a linha:

```sh
DOCKER_OPTS="--iptables=false"
```

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
