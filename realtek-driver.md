### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Driver de rede cabeada Realtek

Meu notebook possui um dispositivo de rede cabeada Realtek, que provavelmente não está incluído no kernel fornecido com o `Ubuntu Studio 22.04 LTS`. Em alguns testes com o `Ubuntu 22.04 LTS` com gnome também percebi o problema. A conexão fica instável e por vezes, nem funciona.

<br>

## Conferir a versão do kernel

```sh
uname -a
```
`~output`
```sh
Linux HP-ENVY 5.15.0-40-lowlatency #43-Ubuntu SMP PREEMPT Thu Jun 16 17:07:13 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
```

<br>

---

## Identificar o dispositivo de rede cabeada Realtek

```sh
lspci | grep Ethernet
```
`~output`
```sh
09:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 10)
```

<br>

## Instalação do driver Realtek

```sh
sudo apt install -y r8168-dkms
```

<br>

Após a instalação é interessante reiniciar o sistema para conferir se está tudo funcionando como deveria, com a conexão estável e desempenho normal.

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
