# dev-ambiente-prepare

O conteúdo deste projeto foi criado a partir de uma instalação limpa do __Ubuntu Studio 22.04 LTS__ e do __Ubuntu 22.04 LTS__ com interface Gnome.

O principal objetivo é documentar tudo que será necessário para preparar um computador com essa instalação, de maneira que ele se torne uma estação de trabalho pra desenvolvimento de sistemas utilizando as mais diversas tecnologias, entre elas: Java, Angular, React Native, PostgreSql, Docker, entre outras.

Teorias, escovação de bits, como as coisas funcionam por baixo do capô e demais detalhes que complicam nossa vida não estão em foco aqui. 

Uma vez funcionando e com a estação de trabalho pronta para produzir, cabe a cada um, de acordo com o interesse e afinidade, se especializar e mergulhar nos detalhes que fazem a mágica acontecer.


A idéia é "Fazer funcionar, sem trauma e sem teoria!".

Vovolinux.

---

Vamos partir da premissa de que as instruções e comandos funcionam (teoricamente), em todas os 'flavors' da versão 22.04 LTS do Ubuntu. Exceções e diferenças pontuais serão identificadas conforme ocorrerem ou forem descobertas.

Este guia foi testado com sucesso no __Ubuntu 22.04 LTS__ com interface Gnome em 24/07/2022.

<br>

---

> Quanta informação! Criei esse índice para auxiliar nas pesquisas e no direcionamento do projeto.
> 
> A idéia inicial era fazer umas anotações úteis para mim e para quem mais quisesse fazer uma consulta rápida sobre "como fazer" as coisas.
> 
> À medida em que fui anotando, o projeto começou a tomar alguma proporção e, com a quantidade de informação que foi produzida, eu precisava saber por onde já tinha passado e o que faltava fazer.

<br>

### Navigation

##### [README: dev-ambient-prepare](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/README.md#dev-ambiente-prepare)

##### [Getting started](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/getting-started.md#getting-started)
- [Atualizar repositórios](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/getting-started.md#atualizar-reposit%C3%B3rios)
  
##### [Driver de rede cabeada Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#driver-de-rede-cabeada-realtek)
- [Conferir a versão do kernel](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#conferir-a-vers%C3%A3o-do-kernel)
- [Identificar o dispositivo de rede cabeada Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#identificar-o-dispositivo-de-rede-cabeada-realtek)
- [Instalação do driver Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#instala%C3%A7%C3%A3o-do-driver-realtek)

##### [Dev essentials](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#dev-essentials)
- [O que é git?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#git)
- [O que é curl?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#curl)
- [O que é docker-compose?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#docker-compose)
- [O que é ubuntu-restricted-extras?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#ubuntu-restricted-extras)
- [O que é gnome-disk-utility?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#gnome-disk-utility)
- [O que é gparted?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#gparted)
- [O que é tree?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#tree)
- [O que é htop?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#htop)
- [O que é neofetch?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#neofetch)
- [O que é terminator?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#terminator)
- [O que é flameshot?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#flameshot)

##### [Desabilitar o início automáGico do serviço docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#desabilitar-o-in%C3%ADcio-autom%C3%A1gico-do-servi%C3%A7o-docker)
- [Desabilitar início dos serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#desabilitar-in%C3%ADcio-dos-servi%C3%A7os-docker)
- [Executar serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#executar-servi%C3%A7os-docker)
- [Interromper serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#interromper-servi%C3%A7os-docker)
- [Habilitar início dos serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#habilitar-in%C3%ADcio-dos-servi%C3%A7os-docker)
- [Impedir o docker de alterar diretamente o iptables](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#impedir-o-docker-de-alterar-diretamente-o-iptables)


##### [Isolar o ambiente de trabalho](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#isolar-o-ambiente-de-trabalho)
- [Criar diretório centralizador (workspace)](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#criar-diret%C3%B3rio-centralizador-workspace)
- [Criar link simbólico para projetos](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#criar-link-simb%C3%B3lico-para-projetos)

##### [Chaves SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#chaves-ssh)
- [Criar um par de chaves SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-um-par-de-chaves-ssh)
- [Incluir uma chave SSH no GitLab](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#incluir-uma-chave-ssh-no-gitlab)
- [Exibir conteúdo da chave pública SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#exibir-conte%C3%BAdo-da-chave-p%C3%BAblica-ssh)
- [Utilizar autenticação com chave SSH em segundo plano](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#utilizar-autentica%C3%A7%C3%A3o-com-chave-ssh-em-segundo-plano)
- [Executar ssh-agent em segundo plano](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#executar-ssh-agent-em-segundo-plano)
- [Adicionar chave privada SSH ao ssh-agent](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#adicionar-chave-privada-ssh-ao-ssh-agent)
- [Criar alias para autenticação com chave SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-alias-para-autentica%C3%A7%C3%A3o-com-chave-ssh)

##### [Clonar repositório utilizando chave SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/clone-repository-with-ssh.md#clonar-reposit%C3%B3rio-utilizando-chave-ssh)

##### Instalar ferramentas
- [Instalar Visual Studio Code](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/install-visual-studio-code.md#instalar-visual-studio-code)
- [Instalar zsh, powerline-fonts e oh-my-zsh](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/zsh-magic-install.md#instalar-zsh-powerline-fonts-e-oh-my-zsh)
- [Instalar OpenJDK](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-openjdk.md#instalar-openjdk)
  - [Verificar versão instalada do Java](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-openjdk.md#verificar-vers%C3%A3o-instalada-do-java)
- [Instalar VirtualBox](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/utils/install-virtualbox.md#instalar-virtualbox)
- [Instalar OpenVpn](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/utils/install-openvpn.md#instalar-openvpn)
- [Instalar Node.js](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-nodejs.md#instalar-nodejs)
  - [Fazer o download do arquivo .deb da versão LTS do Node.js](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-nodejs.md#fazer-o-download-do-arquivo-deb-da-vers%C3%A3o-lts-do-nodejs)
  - [Instalar o pacote nodejs.deb](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-nodejs.md#instalar-o-pacote-nodejsdeb)
  - [Verificar a versão instalada do Node.js](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-nodejs.md#verificar-a-vers%C3%A3o-instalada-do-nodejs)
  - [Verificar a versão instalada do npm](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-nodejs.md#verificar-a-vers%C3%A3o-instalada-do-npm)
- [Instalar Angular 13](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-angular.md#instalar-angular-13)
  - [Verificar a versão instalada do Angular](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/angular/install-angular.md#verificar-a-vers%C3%A3o-instalada-do-angular)
- [Instalar DBeaver Community](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/install-dbeaver.md#instalar-dbeaver-community)

##### Instalar IDE's
- [Instalar IntelliJ IDEA](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-intellij-idea.md#instalar-intellij-idea)
  - [Criar diretório para softwares](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-intellij-idea.md#criar-diret%C3%B3rio-para-softwares) 
  - [Extrair compactado com tar.gz](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-intellij-idea.md#extrair-compactado-com-targz)
  - [Executar IntelliJ IDEA pelo terminal](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-intellij-idea.md#executar-intellij-idea-pelo-terminal)
  - [Criar atalho para IntelliJ IDEA](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-intellij-idea.md#criar-atalho-para-intellij-idea)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.

