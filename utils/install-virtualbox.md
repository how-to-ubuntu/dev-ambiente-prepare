### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar VirtualBox

Para algumas situações o __VirtualBox__ ainda é uma boa solução apesar da existência do __docker__ suprir boa parte das necessidades de desenvolvimento.

Primeiro eu atualizo a lista de pacotes disponíveis nos repositórios:

```sh
sudo apt update
```

<br>

Agora vou instalar o virtualbox, guest additions, extension-pack e recursos de kernel em um único comando:

```sh
sudo apt install -y \
virtualbox virtualbox-ext-pack \
virtualbox-guest-additions-iso virtualbox-guest-utils virtualbox-guest-x11 \
virtualbox-dkms virtualbox-source libvirt-daemon-driver-vbox virtualbox-qt
```

<br>

Durante a instalação será exibida uma mensagem informativa de configuração do `virtualbox-ext-pack`. Basta teclar `TAB` até o `OK` e depois teclar um `espaço`.
    
![img/install-virtualbox-info-ext-pack.png](./img/install-virtualbox-info-ext-pack.png)

<br>

Utilize `TAB` para navegar e `espaço` para selecionar `<Yes>` para aceitar os termos da licença

![img/install-virtualbox-accept-license.png](./img/install-virtualbox-accept-license.png)

<br>


Após essas instalações recomendo fazer uma atualização do sistema apenas para garantir que está tudo funcionando corretamente e que não existem atualizações do sistema relacionadas ao __VirtualBox__.

```sh
sudo apt update && sudo apt upgrade -y
```

<br>

O VirtualBox já deve aparecer disponível no menu de aplicativos do sistema.

Para ver qual versão foi instalada, executeo o VirtualBox, acesse o menu  `Help > About VirtualBox...`

![img/install-virtualbox-check-version.png](./img/install-virtualbox-check-version.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.

