### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar OpenVpn

Para ter acesso à redes externas como se fossem redes locais, podemos utilizar o OpenVPN.

A instalação é bem simples:

```sh
sudo apt install -y openvpn
```

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.