### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Chaves SSH

Clientes de repositórios __Git__ como o __GitHub__, o __GitLab__ e o __BitBucket__, utilizam de chaves __SSH__ para autenticar usuários. Servidores remotos de clientes também podem utilizar esse tipo de autenticação.

Por segurança, sempre que reinstalo o sistema operacional ou crio uma nova conta em um servidor, ou mesmo quando crio um novo projeto em um cliente Git, eu crio uma nova chave.

> Neste exemplo, por se tratar de uma instalação nova, eu vou criar uma chave para o Ubuntu Studio recém instalado em meu computador de trabalho.


## Criar um par de chaves SSH

> Aqui estão os meus dados e os dados do meu ambiente e computador, mas essas variáveis podem ser alteradas para se adequar ao seu caso de uso.

```sh
ssh-keygen -t rsa
```

Em `Enter file in which to save the key (/home/vin/.ssh/id_rsa):` informo o caminho absoluto onde o par de chaves será salvo e o nome desse par de chaves.

O meu ficou assim: `/home/vin/workspace/.ssh/id_vin_ubuntu_studio`

Onde:
- `/home/vin/workspace/.ssh` é o local onde armazeno e centralizo minhas chaves em uso
- `id_vin` refere-se ao meu nome de usuário no Ubuntu Studio recém instalado
- `ubuntu_studio` é o sistema operacional para o qual estou criando o par de chaves SSH

<br>

Em seguida informo uma vez a senha e depois repito a senha para confirmar. 

### IMPORTANTE: Informe sempre uma senha para a sua chave! ###

<br>

---

<br>

## Incluir uma chave SSH no GitLab

Agora vou incluir a minha chave pública nas configurações da minha conta no __GitLab__


### Exibir conteúdo da chave pública SSH

> Neste exemplo, utilizo o caminho e o nome da minha chave SSH. Substitua o caminho e o nome da chave de acordo com a sua realidade.

Execute o comando __(1)__
```sh
cat /home/vin/workspace/.ssh/id_vin_ubuntu_studio.pub
```

<br>

Selecione todo o texto da saída do comando __(2)__ e copie a saída do comando __(3)__ 

![img/ssh-keys-public-key.png](./img/ssh-keys-public-key.png)

<br>

Clique no seu avatar de perfil __(1)__  e depois em __Preferences__ __(2)__

![img/ssh-keys-preferences.png](./img/ssh-keys-preferences.png)

<br>

Clique no menu __SSH Keys__ __(1)__ , cole a chave pública que copiou do terminal no campo __Key__ __(2)__, informe um nome para a chave que seja de fácil identificação para você __(3)__, e depois clique em __Add key__ __(4)__ 

![img/ssh-keys-add.png](./img/ssh-keys-add.png)

<br>


### Utilizar autenticação com chave SSH em segundo plano

Sempre que eu preciso interagir com um repositório remoto a partir de um repositório local no __GitLab__ por exemplo, preciso fazer a autenticação do meu usuário utilizando a minha chave SSH.

São dois comandos simples: um inicia o ssh-agent em segundo plano e o outro adiciona a minha chave privada SSH ao ssh-agent para que, sempre que uma aplicação (*nesse caso o Git*) solicitar uma autenticação, o ssh-agen cuida do processo de validar e assinar a minha chave junto à aplicação. 

#### Executar ssh-agent em segundo plano

```sh
eval "$(ssh-agent -s)"
```

#### Adicionar chave privada SSH ao ssh-agent

```sh
ssh-add /home/vin/workspace/.ssh/id_vin_ubuntu_studio
```

#### Criar alias para autenticação com chave SSH

Apesar de serem dois comandos bem simples, é possível facilitar ainda mais criando um __alias__ com os dois comandos juntos.

Se estiver utlizando o bash, eu edito o arquivo `~/.bashrc`. Se estiver utilizando o zsh, então o arquivo a ser editado é o `~/.zshrc`.

Basta incluir o __alias__ no arquivo conforme abaixo:

```sh
alias gitssh="eval \"$(ssh-agent -s)\" && ssh-add /home/vin/workspace/.ssh/id_vin_ubuntu_studio"
```

Para utilizar o __alias gitssh__ basta fechar o terminal e abrir novamente. Se preferir atualizar a sessão atual com o novo alias, posso executar o comando:

`~ para bash`
```sh
source ~/.bashrc
```

<br>

`~ para zsh`
```sh
source ~/.zshrc
```

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
