### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar zsh, powerline-fonts e oh-my-zsh

Eu mantenho desde 2019 um script shell que faz a instalação do zsh, do powerline-fonts e do oh-my-zsh com tema Fino configurado, automaGicamente.

Este script vai instalar o zsh e o configurador oh-my-zsh, mudar o terminal padrão para zsh e setar o tema Fino para o usuário que estiver executando o script.

Sempre que eu reinstalo um sistema operacional, seja ele servidor ou estação de trabalho eu executo esse script.


## Instalação

Faça o download no terminal com o comando:

```sh
wget -O /tmp/zsh-magic-install https://raw.githubusercontent.com/Viniciusalopes/zsh-magic-install/master/zsh-magic-install
```

<br>

> Execute o script uma vez para cada usuário que desejar instalar o zsh.

<br>

Para instalar para o seu usuário do sistema, apenas digite:

```sh
bash /tmp/zsh-magic-install
```

<br>

Para instalar para o usuário r00t, então digite:

```sh
sudo su
```

e depois:

```sh
bash /tmp/zsh-magic-install
```

<br>

> Mais detalhes e outras informações, veja no [repositório oficial do zsh-magic-install](https://github.com/Viniciusalopes/zsh-magic-install).

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
