### Navigation

##### [README: dev-ambient-prepare](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/README.md#dev-ambiente-prepare)

##### [Getting started](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/getting-started.md#getting-started)
- [Atualizar repositórios](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/getting-started.md#atualizar-reposit%C3%B3rios)
  
##### [Driver de rede cabeada Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#driver-de-rede-cabeada-realtek)
- [Conferir a versão do kernel](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#conferir-a-vers%C3%A3o-do-kernel)
- [Identificar o dispositivo de rede cabeada Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#identificar-o-dispositivo-de-rede-cabeada-realtek)
- [Instalação do driver Realtek](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/realtek-driver.md#instala%C3%A7%C3%A3o-do-driver-realtek)

##### [Dev essentials](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#dev-essentials)
- [O que é git?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#git)
- [O que é curl?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#curl)
- [O que é docker-compose?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#docker-compose)
- [O que é ubuntu-restricted-extras?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#ubuntu-restricted-extras)
- [O que é gnome-disk-utility?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#gnome-disk-utility)
- [O que é gparted?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#gparted)
- [O que é tree?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#tree)
- [O que é htop?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#htop)
- [O que é neofetch?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#neofetch)
- [O que é terminator?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#terminator)
- [O que é flameshot?](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/dev-essentials.md#flameshot)

##### [Desabilitar o início automáGico do serviço docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#desabilitar-o-in%C3%ADcio-autom%C3%A1gico-do-servi%C3%A7o-docker)
- [Desabilitar início dos serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#desabilitar-in%C3%ADcio-dos-servi%C3%A7os-docker)
- [Executar serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#executar-servi%C3%A7os-docker)
- [Interromper serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#interromper-servi%C3%A7os-docker)
- [Habilitar início dos serviços docker](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#habilitar-in%C3%ADcio-dos-servi%C3%A7os-docker)
- [Impedir o docker de alterar diretamente o iptables](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/docker-compose-and-service.md#impedir-o-docker-de-alterar-diretamente-o-iptables)


##### [Isolar o ambiente de trabalho](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#isolar-o-ambiente-de-trabalho)
- [Criar diretório centralizador (workspace)](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#criar-diret%C3%B3rio-centralizador-workspace)
- [Criar link simbólico para projetos](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#criar-link-simb%C3%B3lico-para-projetos)

##### [Chaves SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#chaves-ssh)
- [Criar um par de chaves SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-um-par-de-chaves-ssh)
- [Incluir uma chave SSH no GitLab](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#incluir-uma-chave-ssh-no-gitlab)
- [Exibir conteúdo da chave pública SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#exibir-conte%C3%BAdo-da-chave-p%C3%BAblica-ssh)
- [Utilizar autenticação com chave SSH em segundo plano](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#utilizar-autentica%C3%A7%C3%A3o-com-chave-ssh-em-segundo-plano)
- [Executar ssh-agent em segundo plano](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#executar-ssh-agent-em-segundo-plano)
- [Adicionar chave privada SSH ao ssh-agent](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#adicionar-chave-privada-ssh-ao-ssh-agent)
- [Criar alias para autenticação com chave SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-alias-para-autentica%C3%A7%C3%A3o-com-chave-ssh)

##### [Clonar repositório utilizando chave SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/clone-repository-with-ssh.md#clonar-reposit%C3%B3rio-utilizando-chave-ssh)

##### Instalar ferramentas
- [Instalar Visual Studio Code](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/install-visual-studio-code.md#instalar-visual-studio-code)
- [Instalar zsh, powerline-fonts e oh-my-zsh](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/zsh-magic-install.md#instalar-zsh-powerline-fonts-e-oh-my-zsh)
- [Instalar OpenJDK](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-openjdk.md#instalar-openjdk)
- [Verificar versão instalada do Java](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/java/install-openjdk.md#verificar-vers%C3%A3o-instalada-do-java)
<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)
