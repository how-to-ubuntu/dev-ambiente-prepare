### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar DBeaver Community

O DBeaver é uma excelente ferramenta 'cliente' de bancos de dados. Permite administrar a maioria dos tipos de bancos.

[Link para downloads do site oficial](https://dbeaver.io/download/)

[Link para download do pacote `.deb` para instalação da versão mais recente do __DBeaver Community__](https://dbeaver.io/files/dbeaver-ce_latest_amd64.deb)

<br>

Comando para instalar o pacote

> Neste exemplo eu fiz o download da versão `22.1.1`

```sh
sudo dpkg -i dbeaver-ce_22.1.1_amd64.deb
```

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.

