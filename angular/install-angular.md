### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar Angular 13

```sh
sudo npm install --location=global @angular/cli@13
```

<br>

## Verificar a versão instalada do Angular

```sh
ng v
```

`~output`

![img/install-angular-check-version.png](./img/install-angular-check-version.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.

