### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar Node.js

[Acesso o site oficial para verificar qual é a versão LTS mais recente do Node.js](https://nodejs.org/en/download/)

Na ocasião da criação deste *how-to*, a versão era a `16.16.0`

![img/install-nodejs-lts-version.png](./img/install-nodejs-lts-version.png)

<br>

## Fazer o download do arquivo `.deb` da versão LTS do Node.js

__Link para as versões 16.x__: [https://deb.nodesource.com/node_16.x/pool/main/n/nodejs/](https://deb.nodesource.com/node_16.x/pool/main/n/nodejs/)

__Link direto para a versão `16.16.0` para amd64__: [https://deb.nodesource.com/node_16.x/pool/main/n/nodejs/nodejs_16.16.0-deb-1nodesource1_amd64.deb](https://deb.nodesource.com/node_16.x/pool/main/n/nodejs/nodejs_16.16.0-deb-1nodesource1_amd64.deb)

<br>

## Instalar o pacote nodejs.deb

```sh
sudo dpkg -i nodejs_16.16.0-deb-1nodesource1_amd64.deb
```

## Verificar a versão instalada do Node.js

```sh
node -v
```

<br>

## Verificar a versão instalada do __npm__

```sh
npm -v
```

`~output`

![img/install-nodejs-check-version.png](./img/install-nodejs-check-version.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
