### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar IntelliJ IDEA

Eu crio o diretório `/workspace/software/` para centralizar a maioria dos softwares utilizados para desenvolvimento que executam a partir de binários, ou seja, não precisam de instalação. Isso ajuda a manter o [isolamento do ambiente de trabalho](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/isolated-workspace.md#isolar-o-ambiente-de-trabalho).

## Criar diretório para softwares

```sh
mkdir -pv ~/workspace/software
```

<br>

Fazer o download do __Intellij IDEA__ no diretório `~/workspace/software/`.

[Download do Intellij IDEA Community](https://www.jetbrains.com/pt-br/idea/download/#section=linux)

<br>

Se fizer o download em outro diretório (`~/Downloads/` por exemplo), então deve-se mover o arquivo para o diretório `~/workspace/software/`.

```bash
mv -v ~/Downloads/arquivo.tar.gz ~/workspace/software/

```
<br>

# Extrair compactado com tar.gz

Dentro do diretório `~/workspace/software/` executar o comando:

```sh
tar -xvzf arquivo.tar.gz
```

Após a extração será criado um diretório com o nome da versão do Intellij IDEA baixado. No meu caso o nome do diretório foi `idea-IC-221.5921.22`.

<br>

## Executar IntelliJ IDEA pelo terminal

Para executar o Intellij pela primeira vez, executar o arquivo `/home/vin/workspace…-IC-221.5921.22/bin/idea.sh` 

Marque a opção de __Aceite__ do contrato __(1)__ e clique em __Continue__ __(2)__

![img/install-intellij-idea-accept.png](./img/install-intellij-idea-accept.png)

<br>

Clique em um dos botões para escolher se quer ou não enviar estatísticas para a equipe de desenvolvimento do Intellij

![img/install-intellij-idea-data-sharing.png](./img/install-intellij-idea-data-sharing.png)

O Intellij deve abrir logo em seguida

![img/install-intellij-idea-welcome.png](./img/install-intellij-idea-welcome.png)

<br>

## Criar atalho para IntelliJ IDEA

Independentemente da interface que estiver utilizando no __Ubuntu 22.04__ essa dica deve funcionar sem problemas

Em `~/workspace/software/idea-IC-versao.do.intellij/bin/` eu crio o arquivo `intellij-idea.desktop` com o seguinte conteúdo:

```sh
[Desktop Entry]
Categories=Development;
Comment[en_US]=
Comment=
Exec=bash /home/vin/workspace/software/idea-IC-versao.do.intellij/bin/idea.sh
GenericName[en_US]=Intellij IDEA
GenericName=Intellij IDEA
Icon=/home/vin/workspace/software/idea-IC-versao.do.intellij/bin/idea.svg
MimeType=
Name[en_US]=Intellij IDEA
Name=Intellij IDEA
Path=/home/vin/workspace/projects
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
Version=1.0
X-DBUS-ServiceName=
X-DBUS-StartupType=none
X-KDE-SubstituteUID=false
X-KDE-Username=

```

Depois faço uma cópia desse atalho para os diretórios de applications do meu sistema:
```sh
sudo cp -v ~/workspace/software/idea-IC-versao.do.intellij/bin/intellij-idea.desktop /usr/share/applications/ && 
cp -v ~/workspace/software/idea-IC-versao.do.intellij/bin/intellij-idea.desktop ~/.local/share/applications/
```

Após reiniciar o sistema ou a sessão, o ícone para o __Intellij IDEA__ já é exibido no menu de aplicativos do sistema.

<br>

> Ubuntu Studio

![image/install-intellij-idea-icon-application-studio.png](./image/install-intellij-idea-icon-application-studio.png)

<br>

> Ubuntu com Gnome

![image/install-intellij-idea-icon-application-gnome.png](./image/install-intellij-idea-icon-application-gnome.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
