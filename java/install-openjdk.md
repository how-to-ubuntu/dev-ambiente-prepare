### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar OpenJDK

```sh
sudo apt update && \
sudo apt install -y openjdk-11-jre
```

<br>

## Verificar versão instalada do Java

```sh
java -version
```

<br>

Saída esperada após a instalação do OpenJDK 11

![img/install-openjdk-check-version.png](./img/install-openjdk-check-version.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
