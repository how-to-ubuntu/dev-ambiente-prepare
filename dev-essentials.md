### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Dev essentials

Alguns programas são imprescindíveis para qualquer ambiente de desenvolvimento. Adicionei aqui alguns extras e utilitários que podem facilitar a nossa vida no dia-a-dia.


Em um único comando, vamos instalar os mais básicos. Mais abaixo tem uma breve descrição de cada um deles.

```sh
sudo apt update && \
sudo apt install -y git curl docker-compose \
ubuntu-restricted-extras gnome-disk-utility gparted tree htop \
neofetch terminator flameshot
```

### git

É o software de versionamento de código que é imprescindível ao desenvolvimento de software.

<br>

### curl

É uma ferramenta de linha de comando para transferir dados com sintaxe de URL

<br>

### docker-compose

docker-compose é um software de gerenciamento de serviços construído sobre o docker. Instalando esse pacote, é instalado o docker propriamente, junto com todas as dependências necessárias para trabalhar com containers.

<br>

### ubuntu-restricted-extras

É uma coleção de pacotes que inclui:
 • MP3 e outros softwares de codec de áudio para reproduzir vários formatos de áudio (plugins GStreamer)
 • software para instalar as fontes da Web da Microsoft
 • o plug-in Adobe Flash
 • LAME, software para criar arquivos de áudio compactados.

<br>

### gnome-disk-utility

GNOME Disks é uma ferramenta para gerenciar unidades de disco e mídia.

<br>

### gparted

O GParted é utilizado para detectar e manipular dispositivos e tabelas de partição,

<br>

### tree

Tree é um comando de listagem de diretório recursivo que produz uma lista de arquivos com recuo de profundidade. Utilizado em terminal.

<br>

### htop

Htop é um visualizador de processos baseado em ncursed semelhante ao top, mas permite rolar a lista verticalmente e horizontalmente para ver todos os processos e suas linhas de comando completas.

Interessante também para gerenciar processos.

<br>

### neofetch

O Neofetch é um script de linha de comando de informações do sistema multiplataforma e fácil de usar que coleta as informações do sistema Linux e as exibe no terminal ao lado de uma imagem, pode ser o logotipo de sua distribuição ou qualquer arte ascii de sua escolha.

<br>

### terminator

Terminator é um pequeno projeto para produzir uma maneira eficiente de preencher uma grande área de espaço de tela com terminais.

O usuário pode ter vários terminais em uma janela e usar atalhos de teclado para alternar entre eles. Consulte a página de manual para obter detalhes.

> O emulador de terminal __Konsole__ que vem pré-instalado no __Ubuntu Studio 22.04__ já possui alguns recursos interessantes como os do __terminator__ mas ainda não separei um tempo para comparar os dois. Um exemplo disso é o *split* de terminais na mesma janela que os dois emuladores de terminal. Mesmo assim eu instalo o terminator para o caso de querer ambientes diferentes de vez em quando.

<br>

### flameshot

Flameshot é um software de captura de tela poderoso e simples de usar. 

Recursos notáveis incluem aparência personalizável, edição de captura de tela no aplicativo, interface D-Bus, suporte experimental GNOME/KDE Wayland, integração com Imgur e suporte para interface GUI e CLI.

---

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
