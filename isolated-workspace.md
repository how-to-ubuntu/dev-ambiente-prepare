### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Isolar o ambiente de trabalho

Para manter as coisas organizadas e saber sempre onde está tudo, é interessante criar um diretório que centralize arquivos, projetos, recursos, manuais e tudo quanto for necessário para produzir sem ter que parar para procurar cada vez que for iniciar uma tarefa.

<br>

### Criar diretório centralizador (workspace) 

Elegi o nome __workspace__ para ser o meu diretório centralizador, mas você pode colocar outro nome de sua preferência (dev, desenv, projects, borboletinha-de-taubaté, etc).
A estrutura de diretórios deve ter uma lógica que te auxilie a localizar as coisas sem ter que ficar pesquisando. 
Minha sugestão:
```sh
workspace/
├── a-project
├── b-project
├── c-project
├── d-utils
├── e-docs
├── f-technology
├── .ssh
└── .vpn
```

Para o propósito desse documento vou criar apenas os diretórios `.ssh`, `.vpn` e `projects`.
```sh
export WORKSPACE_PATH=$HOME/workspace && \
mkdir -pv $WORKSPACE_PATH/.ssh && \
mkdir -pv $WORKSPACE_PATH/.vpn && \
mkdir -pv $WORKSPACE_PATH/projects
```

No diretório `.ssh` armazeno minhas diversas chaves para os diversos acessos que possuo: servidores de clientes, GitLab, GitHub, servidores de projetos, etc.

Assim consigo ter a certeza de que minhas chaves e credenciais não estão espalhadas pelo sistema e também não corro o risco de apagar alguma chave caso tenha que reinstalar o meu sistema, ou refazer a minha `/home/vovo` por algum motivo.

<br>

No diretório `.vpn` armazeno minhas credenciais para acessos em redes externas utilizando vpn.

<br>

No diretório `projects` armazeno os projetos que estiver trabalhando no momento ou que preciso manter periodicamente.

<br>

Periodicamente, recomendo fazer o backup de chaves SSH e acessos e credenciais de vpn em local seguro. Evite sempre armazenar suas chaves e credenciais em nuvens de armazenamento como Google Drive, One Drive, DropBox etc.

<br>

### Criar link simbólico para projetos

Para agilizar a digitação ao navegar até o diretório onde ficam os meus projetos, eu crio um link simbólico

```sh
ln -sv /home/vin/workspace/projects/ /home/vin/projects
```

Assim quando eu precisar acessar o diretório de projetos a partir da minha `home`, basta digitar `cd projects` e já estou lá!

![img/isolated-workspace-simbolic-link.png](./img/isolated-workspace-simbolic-link.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
