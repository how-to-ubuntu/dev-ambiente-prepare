### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Instalar Visual Studio Code

É possível fazer o download do arquivo .deb do instalador do VsCode mas, as lojas de aplicativos do Ubuntu já possuem um pacote snap que me atende bem.
Então seguem as duas opções, ficando a seu critério qual vai querer utilizar:

Link para download do Visual Studio Code: https://code.visualstudio.com/download

Na loja de aplicativos __Discover__ do Ubuntu Studio, basta pesquisar por `vscode` e instalar esse aqui:

![img/snap-vscode.png](./img/snap-vscode.png)

Na loja de aplicativos __Ubuntu Software__ do Ubuntu com Gnome, basta pesquisar por `vscode` e instalar esse aqui:

![image/snap-vscode-ubuntu.png](./image/snap-vscode-ubuntu.png)

<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
