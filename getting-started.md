### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

## Atualizar repositórios

```sh
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y
```

<br>


Manter o sistema atualizado é importante para garantir a segurança e, dependendo do caso, a estabilidade do sistema.

Para facilitar ainda mais a atualização do sistema, eu crio um `alias` no perfil do meu usuário.

Se estiver utlizando o bash, eu edito o arquivo `~/.bashrc`. Se estiver utilizando o zsh, então o arquivo a ser editado é o `~/.zshrc`.

Basta incluir o __alias__ no arquivo conforme abaixo:

```sh
alias up="sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y"
```

Assim, sempre que eu lembrar de atualizar o sistema, basta abrir o terminal e digitar __`up`__


<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
