### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

# Clonar repositório utilizando chave SSH

Parece óbvio, mas julgo importante criar esse tópico para servir de consulta para quem nunca utilizou uma dessas tecnolgias (SSH ou Git) ou mesmo já utilizou mas não se lembra exatamente como se faz.

Para clonar um repositório utilizando chave SSH, é preciso [criar um par de chaves SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-um-par-de-chaves-ssh) e [incluir essa chave SSH no GitLab](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#incluir-uma-chave-ssh-no-gitlab).

Também se faz necessário ter permissão de acesso ao repositório que desejar clonar.

Acessando a página principal do repositório, eu clico em Clone __(1)__ e depois no botão para copiar a url do repositório __(2)__

![img/clone-repository-with-ssh-url.png](./img/clone-repository-with-ssh-url.png)

<br>

Por uma questão de organização pessoal, mantenho a estrutura de diretórios do meus repositórios remotos no __GitLab__ e no meu repositório local (*meu computador*) idênticas.

Para fazer isso eu executo o [alias para autenticação com chave SSH](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare/-/blob/main/ssh-keys.md#criar-alias-para-autentica%C3%A7%C3%A3o-com-chave-ssh)__(1)__ e depois o comando `git clone` passando a `url do repositório` como primeiro argumento e o `diretório de destino` como segundo argumento __(2)__, assim:

`~ 1`
```sh
gitssh
```

<br>

`~ 2`
```sh
git clone git@gitlab.com:how-to-ubuntu/dev-ambiente-prepare.git /home/vin/workspace/projects/how-to-ubuntu/dev-ambient-prepare/
```

<br>

A estrutura final fica assim:

```sh
└── workspace
    └── projects
        └── how-to-ubuntu
            └── dev-ambient-prepare
```
<br>

### [Navigation](https://gitlab.com/how-to-ubuntu/dev-ambiente-prepare#navigation)

---
end.
